import os
import sqlite3
from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash, jsonify, json
import random

app = Flask(__name__)

peers = []

@app.route('/')
def index():
    if len(peers) == 0:
        connections = []
    elif len(peers) == 1:
        connections = [0]
    else:
        connections = []
        numberOfConnections = max(1, int(len(peers)) // 3)
        for i in range(numberOfConnections):
            tmp = random.randint(0, len(peers) - 1)
            while tmp in connections:
                tmp = random.randint(0, len(peers) - 1)
            connections.append(tmp)
    peers.append(len(peers))
    return render_template("index.html", peer_id=len(peers) - 1, peer_value=random.randint(-1000, 1000), peer_connections=connections)


@app.route('/raw')
def index_raw():
    return render_template("index.html")

if __name__ == '__main__':
    app.run()
